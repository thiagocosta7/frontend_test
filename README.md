# Frontend Test

O objetivo do desafio é simples: Montar e estruturar um widget de prateleira de cross-sell utilizando HTML e CSS, seguindo o layout fornecido.
Você poderá exibir e listar os produtos de maneira estática ou consumindo o arquivo "produtos.json", via JavaScript puro ou alguma biblioteca como jQuery, por exemplo.

## Design

- A prateleira apresentada deve seguir o layout contido no arquivo teste_layout.psd
- Baseado neste layout, faça uma adaptação responsiva para celulares.

## Como realizar o teste

- Crie um Readme com uma descrição de como rodar seu projeto
- Descreva as funcionalidades do seu desafio, venda seu peixe! Conte-nos o que utilizou, qual foi a metedologia escolhida. Por exemplo, nos diga se fez a prateleira consumindo o arquivo .json de produtos via JavaScript e/ou, se utilizou o Grunt, quais foram os módulos presentes no seu Gruntfile.js
- Em caso de dúvidas, entre em contato com thiago@pubdesign.com.br

## Dicas
 
- O ícone do botão é o add_shopping_cart, do Material Design. https://material.io/tools/icons/
- A fontes utilizadas são Roboto Condensed Bold (títulos), Roboto Regular (texto corrido) e Roboto Bold (preço e preço parcelado), ambas encontradas no Google Fonts. 
- Amamos CSS responsivo, organizado, modular e feito com pré-processadores. Utilizamos o Grunt aqui na empresa e seguimos a filosofia: desenvolver CSS com SCSS e SMACSS faz BEM, rs
- Tenha atenção com espaçamentos, tamanhos e estilos de fonte. 

## Critérios de avaliação

- Alcançar os objetivos propostos
- Qualidade de código
- Fidelidade ao design proposto
- Adaptação mobile
